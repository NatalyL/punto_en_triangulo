#Autores: Doris Chicaiza, Nataly López

# Programa que calcula el area a partir del determinante, de tres puntos2D cuyas coordenadas se ingresan por evento del mouse,
# para saber si un punto (P) se encuentra dentro o fuera del triangulo
# El programa tambien muestra un grafico de la ubicacion de los puntos en el plano

#importamos librerias necesarias
import cv2
import numpy as np
import time

# inicializamos la variable
contador = 0
# dibujamos los puntos
def mouse_drawing(event, x, y, flags, params):
    global contador
    if event == cv2.EVENT_LBUTTONDOWN:
        # el contenedor almacena el valor del punto
        circles[contador] = x,y
        contador = contador + 1
        #print("Punto ", contador, " : ", "(", x, y, ")")

        if contador == 1:
            # (fondo, texto, posicion, tipo de letra, escala, color, estilo, tipo de linea)
            cv2.putText(cap, 'A', (x + 5, y + 5), 2, 0.4, (0, 255, 0), 1, cv2.LINE_AA)
            print("A: (", x, y, ")")
        elif contador == 2:
            cv2.putText(cap, 'B', (x + 5, y + 5), 2, 0.4, (0, 255, 0), 1, cv2.LINE_AA)
            print("B: (", x, y, ")")
        elif contador == 3:
            cv2.putText(cap, 'C', (x + 5, y + 5), 2, 0.4, (0, 255, 0), 1, cv2.LINE_AA)
            print("C: (", x, y, ")")

            # Coordenadas de los puntos de las esquinas del polígono
            pts = np.array([[circles[0]], [circles[1]], [circles[2]]], np.int32)
            # cambiamos la forma de la matriz
            pts = pts.reshape((-1, 1, 2))
            # dibujamos el triangulo
            cv2.polylines(cap, [pts], True, (255, 0, 0), 2)

        elif contador == 4:
            cv2.putText(cap, 'P', (x + 5, y + 5), 2, 0.4, (255, 255, 0), 1, cv2.LINE_AA)
            print("P: (", x, y, ")")


cap = np.zeros((480, 640, 3), np.uint8)

circles = np.zeros((4,2), np.int)

def isInside():
    #pts = np.float32([circles[0], circles[1], circles[2], circles[3]])
    #  Calculate area of triangle ABC
    area = abs((circles[0][0] * (circles[1][1] - circles[2][1]) + circles[1][0] *
                (circles[2][1] - circles[0][1]) + circles[2][0] * (circles[0][1] - circles[1][1])) / 2.0)
    # Calculate area of triangle PBC
    area1 = abs((circles[3][0] * (circles[1][1] - circles[2][1]) + circles[1][0] *
                 (circles[2][1] - circles[3][1]) + circles[2][0] * (circles[3][1] - circles[1][1])) / 2.0)
    # Calculate area of triangle PAC
    area2 = abs((circles[0][0] * (circles[3][1] - circles[2][1]) + circles[3][0] *
                 (circles[2][1] - circles[0][1]) + circles[2][0] * (circles[0][1] - circles[3][1])) / 2.0)
    # Calculate area of triangle PAB
    area3 = abs((circles[0][0] * (circles[1][1] - circles[3][1]) + circles[1][0] *
                 (circles[3][1] - circles[0][1]) + circles[3][0] * (circles[0][1] - circles[1][1])) / 2.0)
    #calcula el area a partir del determinante de forma manual
    #area = (0.5 * (v[0] * v[3] + v[1] * v[4] + v[2] * v[5] - v[3] * v[4] - v[0] * v[5] - v[1] * v[2]))

    sumaTresTrian= area1 + area2 + area3
    print('-----------------------------')
    print('Area del triangulo ABC = ', area)
    print('Suma del area de los triangulos PBC,PAC,PAB = ', sumaTresTrian)

    if (area == area1 + area2 + area3):
        return True
    else:
        return False

timeout = time.time() + 5 # agregamos un tiempo de 5 segundos
while True:
    if contador == 4 and time.time() > timeout:
        if (isInside()):
            print('-----------------------------')
            print('El punto P esta dentro del triangulo')
        else:
            print('-----------------------------')
            print('El punto P esta fuera del triangulo')
        break

    for i in range(0,4):
        cv2.circle(cap,(circles[i][0], circles[i][1]), 4, (0, 0, 255), -1) # (fondo, coordenadas del centro, radio, color, estilo) del punto

    cv2.imshow("Punto", cap)
    cv2.setMouseCallback("Punto", mouse_drawing)

    cv2.waitKey(1)

cv2.destroyAllWindows()